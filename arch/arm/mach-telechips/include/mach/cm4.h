// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */
#ifndef CM4_H
#define CM4_H

#include <common.h>

void run_cm4_firmware(void);

#endif
