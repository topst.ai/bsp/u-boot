/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef VIOC_DEINTLS_H
#define	VIOC_DEINTLS_H

extern void __iomem *VIOC_DEINTLS_GetAddress(void);
#endif
