// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef ADV7182_H
#define ADV7182_H

#include <videosource_if.h>

extern struct videosource videosource_adv7182;

#endif//ADV7182_H
