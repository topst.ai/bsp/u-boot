// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef ISL79988_H
#define ISL79988_H

#include <videosource_if.h>

extern struct videosource videosource_isl79988;

#endif//ISL79988_H
