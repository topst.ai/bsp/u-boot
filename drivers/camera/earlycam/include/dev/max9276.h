// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef MAX9276_H
#define MAX9276_H

#include <videosource_if.h>

extern struct videosource videosource_max9276;

#endif//MAX9276_H
