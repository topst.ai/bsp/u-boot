// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef CAM_MAIN_H
#define CAM_MAIN_H

extern int tcc_earlycamera_start(void);
extern int tcc_earlycamera_stop(void);

#endif//EARLYCAMERA_MAIN_H
