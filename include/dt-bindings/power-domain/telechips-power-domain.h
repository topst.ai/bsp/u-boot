// SPDX-License-Identifier: (GPL-2.0-or-later OR MIT)
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef DT_BINDINGS_TELECHIPS_POWER_DOMAIN_H
#define DT_BINDINGS_TELECHIPS_POWER_DOMAIN_H

#define PD_ID_CA53		(0U)
#define PD_ID_CM4		(1U)
#define PD_ID_PCIE		(2U)
#define PD_ID_MIPI_DPHY1	(3U)
#define PD_ID_LVDS		(4U)
#define PD_ID_SRVC		(5U)
#define PD_ID_SDM		(6U)

#define PD_ID_MAX		(7U)

#define PD_PWR_DOWN		(0U)
#define PD_PWR_UP		(1U)

#endif /* DT_BINDINGS_TELECHIPS_POWER_DOMAIN_H */
