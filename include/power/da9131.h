// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef DA9131_PMIC_H
#define DA9131_PMIC_H

#define DA9131_PMIC_ADDR	(0xD0 >> 1)
#define DA9131_BUCK2_5		0x2D /* register */
#define DA9131_BUCK2_5_VAL	0x53 /* 0.83V */

#endif /* DA9131_PMIC_H */
